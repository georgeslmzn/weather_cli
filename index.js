#!/usr/bin/env node
import { getArgs } from './helper/args.js'
import { printHelp } from './services/log.service.js';
import { saveKeyValue } from './services/storage.service.js';

const initCLI = () => {
	const args = getArgs(process.argv);

	if(args.h) {
		//Вывод help
		printHelp();
	}
	if(args.s) {
		//Вывод help
		console.log(args.s);
	}
	if(args.t) {
		saveKeyValue('token', args.t)
	}
	// Вывести погоду
};

initCLI();