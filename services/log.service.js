import chalk from "chalk";
import dedent from 'dedent-js';

const printError = (error) => {
	console.log(chalk.bgRed(' ERROR ') + ' ' + error);
}

const printSuccess = (message) => {
	console.log(chalk.bgGreen(' SUCCESS ') + ' ' + message);
}

const printHelp = () => {
	const help = dedent`${chalk.bgCyan(' HELP ')}
	Без параметров - вывод погоды
	-s [CITY] для установки города
	-h вывод помощи
	-t [APIKEY] - для сохранения токена
	`
	console.log(help);
}

export { printError, printSuccess, printHelp }